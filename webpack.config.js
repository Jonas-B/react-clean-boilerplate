var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require('path');

module.exports = {
  context: path.join(__dirname, "src"),
  entry: "./js/App.js",
  output: {
    path: __dirname + "/public/",
    filename: "bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015']
        }
      },
      { 
        test: /\.styl$/, 
        loader:  'style-loader!css-loader!stylus-loader', 
        exclude: /node_modules/ 
      }
    ]
  }
};
